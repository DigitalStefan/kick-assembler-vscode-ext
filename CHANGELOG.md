# CHANGELOG

<!--- next entry here -->

## 0.8.1
2021-09-28

### Fixes

- **memoryview:** show when visible (41ab20bb1df9da6d6d3520ea89e8c68b518276b6)

### Other changes

- Merge branch 'fix-memory-view-vanishes' into 'master' (9c900401d33101bcb41314808ea547d2541320df)

## 0.8.0
2021-09-27

### Features

- **settings:** kickass 5.22 version check support (f740b114413056b69ac58e1e9fe6df816f4a9b54)

### Other changes

- Merge branch 'ka522support' into 'master' (c219432b4d8d12d5ec19837755e0609b97bd6b1f)

## 0.7.17
2021-09-11

### Fixes

- **startup:** validate settings (1efec7ec4d3a9096b02222c6f30aaf801934decd)

### Other changes

- Merge branch 'fix-startup-validation' into 'master' (55976a98a244433c84aa78c1154c96031ea85987)

## 0.7.16
2021-09-08

### Fixes

- **hover:** remove suppression of blank line (e9a6158dbe92691df63fb1f6c97e20ab30ca11fb)

### Other changes

- Merge branch 'enhance-hover-remarks' into 'master' (12024884b2ce54c404f0969c0b1defcea19effde)

## 0.7.15
2021-09-05

### Fixes

- **settings:** support for suppressing informational messages (18ae1ec6b301132256e084b7b125d02841936f1b)

### Other changes

- (fix) support for suppressing informational messages (cf3c11a5165936ee3bcd8c7648783d9224be1ad7)
- Merge remote-tracking branch 'origin/fix-issue-109-supress-messages' into fix-issue-109-supress-messages (1c91701314c8e105139818fc03279dad7a6a1e02)
- Merge branch 'fix-issue-109-supress-messages' into 'master' (71397ff8db5b1ef475e5e82a5c0cff43d36b7748)

## 0.7.14
2021-06-24

### Fixes

- **build:** empty java options breaks build (7752e8404f13bca547db2b7d29caa6961d970fb0)

### Other changes

- Merge branch 'fix-empty-java-options-bug' into 'master' (f88350b209bc8ced9ecbfb45ec6eb3d64fc980f5)

## 0.7.13
2021-06-23

### Fixes

- **assembler:** fix bug from last release (d66b3d0b1f6f13c621055c0a33401d1dfaa88eeb)
- **ci:** vscode ignore was broken in vsce 1.94 (03b87e9d6b6c46d9c2deac9e8b6cc91e499211c4)

### Other changes

- update release notes (67f653273b776ed45295a399e3cf017fdfc2b6e8)
- Merge remote-tracking branch 'origin/master' (c38dba6db67372e73be4d750ba15a3c79ea4d6a9)
- Merge branch 'fix-java-runtime-error' into 'master' (fe62db4619d8218cfb0b9f066667ba603bd21270)
- Merge remote-tracking branch 'origin/master' (c325594f138152f7d1cef78b14080b51bcdc2e20)

## 0.7.12
2021-06-22

### Fixes

- **ci:** remove hard coded version fix (17540665b05288f120578de0547832fdbf3719a7)
- **ci:** missing next release call (45723333cd2bb6122c087d3b067c0f7eb7a5792d)

## 0.7.10
2021-06-22

### Fixes

- **ci:** need to allow same version again (dbb55989e48a8f815e3d57e9b9d50bb83c211db6)

## 0.7.9
2021-06-22

### Fixes

- **ci:** correcting automated versioning (00925646d3f392685adcf937515d0b8ceb686987)

### Other changes

- Update .gitlab-ci.yml (944fcd46a0aaa3ea7f01156b36ca9171546b6e98)

## 0.7.8
2021-06-22

### Fixes

- replaced deprecated assembler settings (965690a01c2aa85478001a87a68d0aefc4fae5ea)
- add new java settings (de4dfae406697c039763adfe31b233837417652f)
- **settings:** use temp file for version checking (c1da8039d1c48e9abb19c261910eb8e86ddab501)
- **version:** bump to correct version (7c6dfbce4793d5403164620838add47beabc01e0)
- **version:** trying to bump version again (3149812bdb832398d4a2e7fe9c3fd57477bf09b3)
- **version:** modify job to bump if no changes present (bb82c2bb8545316e95dabc1a399337edc1d9305a)
- **ci:** remove allowing same version (9195b335527a352a45471c800aa89433380204a4)

### Other changes

- Merge branch 'feature-java-command-options' into 'master' (b324244dec8518cc279f0612441a5129d0674b52)
- Merge remote-tracking branch 'origin/master' (c09ccf906ea7444a3c307a044ce77c0692d3f8b3)
- bump to 0.7.8 (d701245313d4fdbfc3f2510a18a12cc5dbac6530)
- Update package.json (35a6581f6fb1e60462259ad0bc6659d2e9f42a71)
- Update .gitlab-ci.yml (8f9604b05cfb98f43f2f714228a2418413e89056)
- Update .gitlab-ci.yml (91286004f039c6a7aa056da435f5c0c6dd51e50e)
- Update .gitlab-ci.yml (9c808cb9ceba60f1c4cb3ff8c87a7f08244af6b8)
- Update .gitlab-ci.yml (7a4abd68a6d4fac9870f678a4f1052634fdcd7b6)
- Update .gitlab-ci.yml (ec0e53c68a95f27613b5df1ed4880ccf5471354f)
- Update .gitlab-ci.yml (8ef47473c11e6336bfeadc1a79e01c6f3abde19b)

## 0.7.8
2021-06-22

### Fixes
- correcting version number

## 0.7.7
2021-05-05

### Fixes

- **build:** client build breaking when no errors (2bf4c818c1c8d94ec1f71a9e61a555d034c6de82)

### Other changes

- Merge branch 'fix-run-emulators' into 'master' (4ee13352bcd30cf0d2f4559b9be90678e11764de)

## 0.7.6
2021-05-03

### Fixes

- **assembler:** add 5.20 support (edd395c71a2ddfc5c39bb571113b0e838d0cc88c)

### Other changes

- KickAssembler 5.20 support and some fixes (814e75a3ec6856e95f7176d5cb67d480c110ca33)
- Merge branch 'kickAss520Support' into 'master' (379114c4b19e1adf2ef98e9a1b985a7834550034)

## 0.7.5
2021-05-02

### Fixes

- **hover:** catch non existing assemblerinfo (77ccd3611efaf8330686f69094bcd6af362d0294)

### Other changes

- Merge branch 'fix/98/ignoreMissingPrerequesites' into 'master' (9b49d95db871382d903c818014884d643d9e6227)

## 0.7.4
2021-04-16

### Fixes

- **ci:** show changelog before publish (eaa51a67759fa8029868d6ce341565c3b49ae37c)

### Other changes

- fix(ci) : add changelog to build (4816c9974a5f2426e30acf1831e4f7e840a500b7)

## 0.7.3
2021-04-14

### Fixes

- **doc:** update release notes (0fa2141997216614d6f5c877089feb08e6d5a4b2)
- **snippet:** misleading snippet placeholders (daf7853b4951f4546c4fad184f4bc7bd1ac1f92c)

## 0.7.2
2021-04-11

### Fixes

- **references:** macro names were not found (e06849f49b0659d4b0748ed0303d33763ee8ddef)
- **definition:** recognize labels in imm mnemonic (3589e045453c01a65869fff3688efa8612ce94d9)
- **snippets:** errorif snippet misses a comma (3ac7c071bc9e7383ea87ec594411c5c5e1a7f730)
- **diagnostic:** support build errors in includes (d8819678daf0ef47c69681da2e47beb52f03674d)

## 0.7.1
2021-03-31

### Fixes

- **config:** better error checking for key settings (c70573f8d6f9cadd5e09f50997180e2b4894b320)

## 0.7.0
2021-03-27

### Features

- **assembler:** Add -binfile Support to Assembler (a18ac6f8edce2dc9cfb9ea4eee1414f4134fa44a)
- **diagnostic:** show build errors in diagnostic provider (4bf788f7a8c7a92117afbdd232d089bb62e4be90)

### Fixes

- added information for commit messages when contributing (6609a27219f9c6aef8fd1d0ae1707a3f6f50a34e)
- **fs:** some checks were still using old exists method (b58153ec9f916e208475b0c236a29c820e6d62a1)
- **ci:** add changelog to version stage (f8eb6ac1781995126ceb535435d1e55644445f36)
- **ci:** do not include other changes in final changelog (82de68d2b6ec33ce8206bf2d51187e13b7b83c7a)

## 0.6.4
2021-03-25

### Fixes

- **ci:** fix release link (ee1b7db5762aa7f67226c05cd8886d87527d733e)

## 0.6.3
2021-03-25

### Fixes

- **build:** changed version number (409eb86c92940370e28c97960ec1c087fb9ad2ef)

## 0.6.2
2021-03-25

### Fixes

- **folding:** support nested scope folding (37506e77c5719520d3c772ac1259c69fbfab37f5)
- **ci:** added ci build (2e61107fe4312ea16d342f82b6075ca7265ffcc7)
- **build:** added package-locks (c9795047c960c67d856dce86acb8e2f2dd633c07)
