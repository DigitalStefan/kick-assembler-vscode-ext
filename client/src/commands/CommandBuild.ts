/*
	Copyright (C) 2018-2021 Paul Hocker. All rights reserved.
	Licensed under the MIT License. (See LICENSE.md in the project root for license information)
*/


import { spawn, spawnSync } from 'child_process';
import { Uri, workspace, window, Disposable, ExtensionContext, commands, WorkspaceConfiguration, TextDocument } from 'vscode';
import PathUtils from '../utils/PathUtils';  
import * as vscode from 'vscode';
import * as path from 'path';
import ClientUtils from '../utils/ClientUtils';
// import * as fs from 'fs';

export class BuildInformation {
    public buildStatus: number
    public buildData: string
    public buildError: vscode.Diagnostic[] = []
    public buildErrorFile: string
}



export class CommandBuild { 

    public stdout: string;
    
    private configuration: WorkspaceConfiguration;
    private output: vscode.OutputChannel;
    private showOutput : boolean = true;

    constructor(context:ExtensionContext, output:vscode.OutputChannel, showOutput : boolean = true) {
        this.configuration = workspace.getConfiguration('kickassembler');
        this.output = output;
        this.showOutput = showOutput;
    }

    public buildOpen(): BuildInformation {


        var uri : Uri = ClientUtils.GetOpenDocumentUri();
        
        if (!uri) {
            var nostartup = window.showWarningMessage(`Cannot build because there is no open document.`);
            return;
        }

        // return this.build(uri);
        return this.build(null);
    }

    public buildStartup(text:TextDocument): BuildInformation | undefined{

        var _textDocument : TextDocument = text;
        
        if (!_textDocument) {

            var nostartup = window.showWarningMessage(`Cannot build because there is no startup file defined in your Settings.`, { title: 'Open Settings'});

            nostartup.then((value) => {
                if (value){
                    vscode.commands.executeCommand('workbench.action.openSettings', `kickassembler.startup`);
                }
            });

            return;
        }

        // return this.build(uri);
        return this.build(_textDocument);
    }

    public build(text:vscode.TextDocument): BuildInformation {

        // show notifications setting
        let _show_notifications : boolean = this.configuration.get("editor.showNotifications");

        // get the java runtime
        let _java_runtime: string = this.configuration.get("java.runtime");
        if (!_java_runtime) {
            _java_runtime = this.configuration.get("javaRuntime");
        }

        // get the path to the kickass jar
        let _assembler_jar: string = this.configuration.get("assembler.jar");

        // get the assembler main class
        let _assembler_main: string = this.configuration.get("assembler.main");

        // let base = path.basename(sourceFile.fsPath);
        let base = path.basename(text.fileName)

        // get the output filename from the current filename
        let outputFile = path.join(ClientUtils.GetOutputPath(), ClientUtils.CreateProgramFilename(base));

        // remove old program file
        PathUtils.fileRemove(outputFile);

        // create symbol directory
        let symbolDir:string = ClientUtils.GetOutputPath();

        // get the path of the source
        // var source: string = PathUtils.GetPathFromFilename(sourceFile.path);
        // var scheme: string = "";

        // if (sourceFile.scheme != null && sourceFile.scheme != 'file') {
        //     scheme = sourceFile.scheme + ":";
        // }

        // var sourcePath: string = path.join(scheme, source);
        var sourceFile = text.fileName
        var sourcePath = path.dirname(text.fileName)

        // create new output channel
        // this.output.clear();
        this.output.show(true);

        var cpSeparator:string = process.platform == "win32" ? ';' : ':';
        var cpPlugins:string[] = this.configuration.get("javaPlugins");
        var cpPluginParameters:string[] = this.configuration.get("javaPluginSystemProperties");
        cpPluginParameters = cpPluginParameters.map(p => '-D' + p);

        // add custom java options from settings
        let _java_options: [];
        let _java_options_raw: string = this.configuration.get("java.options");

        let javaOptions = [
            "-cp", 
            cpPlugins.join(cpSeparator) + cpSeparator + _assembler_jar,
            ...cpPluginParameters,
            _assembler_main, 
            sourceFile, 
            "-o", 
            outputFile, 
            "-symbolfile", 
            "-symbolfiledir", 
            symbolDir
        ];

        if (_java_options_raw.length > 0) {
            let _raw_split = _java_options_raw.split(" ")
            javaOptions = [..._raw_split, ...javaOptions];
        }

        if (this.configuration.get("debuggerDumpFile")){
            javaOptions.push('-debugdump');
        }

        if (this.configuration.get("byteDumpFile")){
            let byteDumpFile = path.join(ClientUtils.GetOutputPath(), "ByteDump.txt");
            javaOptions.push('-bytedumpfile', byteDumpFile);
        }

        if (this.configuration.get("emulatorViceSymbols")){
            javaOptions.push('-vicesymbols');
        }

        if (this.configuration.get("assembler.option.afo")){
            javaOptions.push('-afo');
        }

        if(this.configuration.get("opcodes.DTV")){
            javaOptions.push('-dtv');
        }

        if(!this.configuration.get("opcodes.illegal")){
            javaOptions.push('-excludeillegal');
        }

        if(this.configuration.get("assembler.option.binfile")) {
            javaOptions.push('-binfile');
        }

        // add library paths for kick assembler
        var libdirPaths:string[] = this.configuration.get("assemblerLibraryPaths");

        libdirPaths.forEach((libPath) => {

            if(!path.isAbsolute(libPath)) {
                libPath = path.join(sourcePath, libPath);
            }

            if (PathUtils.directoryExists(libPath)) {
                javaOptions.push('-libdir',libPath);
            }
        });

        //window.showInformationMessage(`Building ${base.toUpperCase()}`);

        var start = process.hrtime();

        let java = spawnSync(_java_runtime, javaOptions, { cwd: path.resolve(sourcePath), shell: true });

        var end = process.hrtime(start);

        let errorCode = java.status;

        // console.log(javaOptions);

        let time = `(${end[0]}s ${end[1].toString().substr(0,3)}ms)`;

        var errorText: RegExpMatchArray | null,
            errorPosition: RegExpMatchArray | null;

        let _java_output = java.stdout.toString();
        if (errorCode > 0) {
            console.error(java.stderr.toString());
            let _error_output = java.stdout.toString();
            errorText = _error_output.match(/Error: (.*)/);
            errorPosition = _error_output.match(/at line (\d+), column (\d+) in (.*)/);
            window.showWarningMessage(`Build of ${base.toUpperCase()} Failed ${time}`);
        } else {
            if (_show_notifications) {
                window.showInformationMessage(`Build of ${base.toUpperCase()} Complete ${time}`);
            }
        }

        if (this.showOutput) {
            this.output.append(_java_output);
        }

        this.stdout = _java_output;

        var _info = new BuildInformation();
        _info.buildStatus = java.status;
        _info.buildData = java.output.toString();

        if(errorText && errorPosition && errorText.length > 0 && errorPosition.length > 2) {

            let errorMessage = errorText[1];
            let errorLine = parseInt(errorPosition[1],10) - 1;
            let errorColumn = parseInt(errorPosition[2],10) - 1;
            _info.buildErrorFile = errorPosition[3];
            _info.buildError.push({
                severity: vscode.DiagnosticSeverity.Error,
                range: new vscode.Range(
                    new vscode.Position(errorLine,errorColumn),
                    new vscode.Position(errorLine,errorColumn),
                ),
                message: errorMessage,
                source: "kickassembler",
            });
        }

        //fetch all warnings
        let warnings = _java_output.match(/(\(.*) (\d+:\d+)\) Warning: (.*)/g) || [];

        warnings.forEach((warningString) => {
            // split up into details
            let warning = warningString.match(/(\(.*) (\d+:\d+)\) Warning: (.*)/);
            let warningPosition = warning[2].split(':');
            let warningLine = parseInt(warningPosition[0],10) - 1;
            let warningColumn = parseInt(warningPosition[1],10);
            _info.buildError.push({
                severity: vscode.DiagnosticSeverity.Warning,
                range: new vscode.Range(
                    new vscode.Position(warningLine,warningColumn),
                    new vscode.Position(warningLine,warningColumn),
                ),
                message: warning[3],
                source: "kickassembler",
            });
        });
                

        return _info;
    }
}
